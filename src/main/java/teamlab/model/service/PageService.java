package teamlab.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teamlab.model.dao.ActivityDao;
import teamlab.model.dao.PageDao;
import teamlab.model.entity.Page;
import teamlab.model.response.UserPage;
import teamlab.model.response.ActivityUser;

@Service
public class PageService {
    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private PageDao pageDao;

    private static final Logger logger = LoggerFactory.getLogger(PageService.class);

    public List<UserPage> findUserViewedPage(String keyWord) {
        List<Page> pages = new ArrayList<>();

        Map<Integer, List<ActivityUser>> activityUsersMap = new HashMap<>();

        pages = pageDao.findPageByTitle(keyWord);

        if (pages.size() > 0) {
            List<ActivityUser> activityUsers = activityDao.findActivityUserByPageTitle(keyWord);
            // 配列からマップに詰め直す
            for (ActivityUser activityUser : activityUsers) {
                if (activityUsersMap.containsKey(activityUser.getPageId())) {
                    activityUsersMap.get(activityUser.getPageId()).add(activityUser);
                } else {
                    activityUsersMap.put(activityUser.getPageId(), new ArrayList<>(Arrays.asList(activityUser)));
                }
            }
        }

        Map<Integer, UserPage> userPageMap = new HashMap<>();
        List<UserPage> userPageList = new ArrayList<>();

        //ページごとにユーザの閲覧数
        for (int i = 0; i < pages.size(); i++) {
            boolean isFound = false;
            if (activityUsersMap.containsKey(pages.get(i).getId())) {
                List<ActivityUser> activityUsers = activityUsersMap.get(pages.get(i).getId());
                for (int j = 0; j < activityUsers.size(); j++) {
                    isFound = true;
                    UserPage userPage = new UserPage();
                    userPage.pageId = pages.get(i).getId();
                    userPage.pageTitle = pages.get(i).getTitle();
                    userPage.userId = activityUsers.get(j).getUserId();
                    userPage.userName = activityUsers.get(j).getUserName();
                    userPage.viewCount = activityUsers.get(j).getUserCount();
                    userPageMap.put(activityUsers.get(j).getUserId(), userPage);
                }
            }
            if (!isFound) {
                UserPage userPage = new UserPage();
                userPage.pageId = pages.get(i).getId();
                userPage.pageTitle = pages.get(i).getTitle();
                userPageList.add(userPage);
            }

        }

        //ユーザIDでソート
        Object[] mapkey = userPageMap.keySet().toArray();
        Arrays.sort(mapkey);
        for (Integer key : userPageMap.keySet()) {
            userPageList.add(userPageMap.get(key));
        }

        return userPageList;
    }
}
