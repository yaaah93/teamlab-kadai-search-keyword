package teamlab.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import teamlab.model.entity.Page;

public interface PageDao extends JpaRepository<Page, Integer> {

    @Query(value = "select new teamlab.model.entity.Page(p.id, p.title) from Page p where p.title like ?1% order by p.id asc")
    public List<Page> findPageByTitle(String title);
}