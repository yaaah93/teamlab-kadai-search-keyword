package teamlab.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import teamlab.model.entity.Activity;
import teamlab.model.response.ActivityUser;

public interface ActivityDao extends JpaRepository<Activity, Integer> {

	@Query("select a from Activity a where a.pageId in ?1")
	public List<Activity> findActivityByPageId(List<Integer> pageIds);

	@Query(value = "select "
		+ "new teamlab.model.response.ActivityUser(pageId, userId, (select u.name from teamlab.model.entity.User u where u.id = a.userId), count(userId)) "
		+ "from Activity a "
		+ "where pageId in (select p.id from teamlab.model.entity.Page p where p.title like ?1%) "
		+ "group by pageId, userId"
	)
	public List<ActivityUser> findActivityUserByPageTitle(String title);

}