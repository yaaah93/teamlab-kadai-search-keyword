package teamlab.model.response;

public class ActivityUser {
	private Integer pageId;
	private Integer userId;
	private String userName;
	private Long userCount;

	public ActivityUser(Integer pageId, Integer userId, String userName, Long userCount) {
		this.pageId = pageId;
		this.userId = userId;
		this.userName = userName;
		this.userCount = userCount;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public Integer getPageId() {
		return this.pageId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return this.userName;
	}

	public void setUserCount(Long userCount) {
		this.userCount = userCount;
	}
	public Long getUserCount() {
		return this.userCount;
	}
}
