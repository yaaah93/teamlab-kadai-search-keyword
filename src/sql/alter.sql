-- 修正SQL文
ALTER TABLE `page` ADD INDEX index_page_on_title(`title`);
ALTER TABLE `activity` ADD INDEX index_activity_on_page_id_user_id(`page_id`, `user_id`);